﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GitTest203
{
    public partial class Form1 : Form
    {

        private Database db;

        public Form1()
        {
            InitializeComponent();
        }



        private void btnOpen_Click(object sender, EventArgs e)
        {
            db.Connecter();
            labelStatus.Text = db.connection.State.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            db = new Database();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            db.Deconnecter();
            labelStatus.Text = db.connection.State.ToString();
        }
    }
}
