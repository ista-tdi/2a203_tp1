﻿using System.Data.SqlClient;
using System.Data;

namespace GitTest203
{
    class Database
    {
        // Declaration des objets de la SGBD
        public SqlConnection connection = new SqlConnection();


        public void Connecter()
        {
            connection.ConnectionString = "Data Source=DESKTOP-HFLAVK5;Initial Catalog=TP1;Integrated Security=True";
            if(connection.State.Equals(ConnectionState.Closed) ||
                connection.State.Equals(ConnectionState.Broken))
            {
                connection.Open();
            }
            
        }

        public void Deconnecter()
        {
            if (connection.State.Equals(ConnectionState.Open))
            {
                connection.Close();
            }
        }

    }
}
